import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextField;

public class SimulationManager implements Runnable{
	public int timeLimit;
	public int maxProcessingTime;
	public int minProcessigTime;
	public int numberOfServers;
	public int numberOfClients;
	private Scheduler scheduler;
	private ArrayList<Task> generatedTasks=new ArrayList<Task>();
	private SimulationFrame frame=new SimulationFrame();
	public int optiune;
	
	public SimulationManager(int servere,int clienti,int timplimita,int minProcesare,int maxProcesare,int i) {
		numberOfServers=servere;
		numberOfClients=clienti;
		timeLimit=timplimita;
		minProcessigTime=minProcesare;
		maxProcessingTime=maxProcesare;
		optiune=i;
		scheduler=new Scheduler(numberOfServers,numberOfClients);
		generareNTasks();
	}
	
	public void generareNTasks() {
		Random rand=new Random();
		ArrayList<Task> aux=new ArrayList<Task>();
		ArrayList<Task> aux1=new ArrayList<Task>();
		for(int i=0;i<numberOfClients;i++) {
			int processTime=rand.nextInt(maxProcessingTime-(minProcessigTime+1))+minProcessigTime+1;
			int arrivalTime=rand.nextInt(timeLimit);
			Task t=new Task(arrivalTime,processTime);
			aux.add(t);
		}
		aux1.addAll(aux);
		Task t1=new Task(0,0);
		int index=0;
		for(Task t:aux) {
			int minArrival=Integer.MAX_VALUE;
			int nr=0;
			for(Task i:aux1) {
				if(i.getArrivalTime()<minArrival) {
				    t1=new Task(i.getArrivalTime(),i.getProcessingTime());
					minArrival=i.getArrivalTime();
				    index=nr;
				}
				nr=nr+1;
			}
			generatedTasks.add(t1);
			aux1.remove(index);
		}
		for(Task t:generatedTasks) {
			System.out.println(t.toString());
		}
	}
	public void afisare() {
		for(Task t:generatedTasks) {
			System.out.println(t.getArrivalTime()+" "+t.getProcessingTime());
		}
	}
	public void run() {
		int currentTime=0;int i,nr=0,peakTime=0,clientiMax=0;
		Task aux=new Task(0,0);
		while (currentTime<timeLimit+1) {
			for(Task t:generatedTasks) {
				if(currentTime==t.getArrivalTime()) {
					nr=nr+1;
					aux=new Task(t.getArrivalTime(),t.getProcessingTime());
					int waitTime=0,indice=0;;
					i=scheduler.dispatchTask(aux,optiune);
					int nrClientiMax=scheduler.getClientiScheduler();
					if(nrClientiMax>clientiMax) {
						clientiMax=nrClientiMax;
						peakTime=currentTime;
					}
					for(Server aux1:scheduler.servers) {
						if(i==indice) waitTime=aux1.getWaitingTime()-t.getProcessingTime();
						indice=indice+1;
						}
					int finishTime=waitTime+t.getArrivalTime()+t.getProcessingTime();
					frame.setLogger("La coada: "+i+" Clientul "+nr+"- ArrivalTime: "+t.getArrivalTime()+" ProcessingTime: "+t.getProcessingTime()+"FinishTime: "+finishTime);
					frame.setLogger("\n");
				}
			}
			currentTime=currentTime+1;
			frame.setNrClientiServiti(scheduler.getNrClientiServiti()+"");
			frame.displayData(scheduler.getAllTassks(),numberOfServers,numberOfClients);
			frame.setTextTimer(currentTime+"");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		frame.setLogger("Timpul limita de aparatie la cozi s-a terminat:"+timeLimit);
		frame.setLogger("\n");
		int max=Integer.MIN_VALUE;
		for(Server s:scheduler.servers)
			if (s.getWaitingTime()>max) max=s.getWaitingTime();
		while(max+1>0) {
			currentTime=currentTime+1;
			try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		frame.setNrClientiServiti(scheduler.getNrClientiServiti()+"");
		frame.displayData(scheduler.getAllTassks(),numberOfServers,numberOfClients);
		frame.setTextTimer(currentTime+"");
		max=max-1;
	}
		frame.setLogger("Nu mai sunt clienti in magazin.");
		frame.setLogger("\n");
		frame.setLogger("Peak time a fost:"+peakTime+" cu numarul de clienti:"+clientiMax);
	}
}
