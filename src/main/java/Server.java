import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public class Server implements Runnable {
	public Vector<Task> tasks;
	private AtomicInteger waitingPeriod;
	public int nrClienti=0;
	public int nrTaskMax;
	Server(int nrTask){
		nrTaskMax=nrTask;
		tasks=new Vector<Task>(nrTask);
		Task t=new Task(0,0);
		tasks.add(t);
		waitingPeriod=new AtomicInteger(1);
	}
	public synchronized void addTask(Task newTask) {
		tasks.add(newTask);
		waitingPeriod.addAndGet(newTask.getProcessingTime());
		notifyAll();
	}
	public void run() {
		Task aux=new Task(0,1);
		while((aux.getProcessingTime()>0)){
				waitingPeriod.addAndGet(-1);
				aux.setProcessing(aux.getProcessingTime()-1);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			    if(aux.getProcessingTime()==0) {
			    	tasks.removeElementAt(0);
			    	nrClienti=nrClienti+1;
			    	if(tasks.size()==0) {
					while(tasks.size()==0)
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
			    	aux=tasks.firstElement();
						}
				else 
					aux=tasks.firstElement();
			    }
				else
				tasks.set(0, aux);
			    
		}
	}
	public int getWaitingTime() {
		return waitingPeriod.intValue();
	}
	public int getNrTaskMax() {
		return nrTaskMax;
	}
	public int getSize() {
		return tasks.size();
	}
	public Task[] getTasks() {
		Task[] t=new Task[tasks.size()];
		int i=0;
		for(Task aux:tasks) {
		    t[i]=aux;
			i++;
		}
		return t;
	}
}
