import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class SimulationFrame extends JFrame{
	private JPanel panel;
	private int width=600,height=600;
	private JTextArea area=new JTextArea(5,30);
	private JTextArea area1=new JTextArea(5,40);
	private JTextField t=new JTextField(3);
	private JTextField t1=new JTextField(3);
	public SimulationFrame() {
		panel=new JPanel();
		this.setSize(width, height);
		JLabel l1=new JLabel("Magazin");
		JLabel l3=new JLabel("Clienti Serviti");
		JLabel l2=new JLabel("Timer:");
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JScrollPane sc=new JScrollPane(area1);
		sc.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		p1.add(l1);
		p1.add(area);
		p2.add(l3);
		p2.add(t1);
		p2.add(l2);
		p2.add(t);
		p3.add(sc);
		
		panel. setLayout (new BoxLayout (panel, BoxLayout . Y_AXIS ));
		panel.add(p1);
		panel.add(p2);
		panel.add(p3);
		this.add(panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void setTextTimer(String s) {
		t.setText(s);
	}
	public void setNrClientiServiti(String s) {
		t1.setText(s);
}
	public void setLogger(String s) {
		area1.append(s);
	}
	public void displayData(Task[][] task,int maxServer,int maxClient) {
		int i,j;
		String s=new String();
		for(i=0;i<maxServer;i++) {
			s=s+" Coada "+i+": ";
			for(j=0;j<maxClient;j++)
				if(task[i][j].getProcessingTime()!=0)
			 s=s+task[i][j].getProcessingTime()+",";
			s=s+"\n";
		}
		area.setText(s);
		this.revalidate();

}
}
