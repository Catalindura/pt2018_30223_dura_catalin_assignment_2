import java.util.ArrayList;

public class Scheduler {
	public ArrayList<Server> servers=new ArrayList<Server>();
	private int maxNoServers;
	private int maxTasksPerServer;
	public Scheduler(int NrServere,int tasks) {
		maxTasksPerServer=tasks;
		maxNoServers=NrServere;
		servers=new ArrayList<Server>();
		for(int i=0;i<NrServere;i++) {
			Server aux=new Server(tasks);
			servers.add(aux);
			Thread t=new Thread(aux);
			t.start();
	}
	}
	public synchronized int dispatchTask(Task t,int optiune) {
		if(optiune==0) {
		int min=Integer.MAX_VALUE;
		int index=0,i=0;
		for(Server s:servers) {
			if((s.getWaitingTime()<min)&(s.getNrTaskMax()!=s.getSize())) {
				min=s.getWaitingTime(); 
				index=i;
			}
			i=i+1;
		}
		i=0;
		for(Server s:servers) {
			if(i==index) {
				s.addTask(t);
			}
			i=i+1;
		}
		return index;
		}
		else {
			int min=Integer.MAX_VALUE;
			int index=0,i=0;
			for(Server s:servers) {
				if(s.getSize()<min) { 
					min=s.getSize(); 
					index=i;
				}
				i=i+1;
			}
			i=0;
			for(Server s:servers) {
				if(i==index) {
					s.addTask(t);
				}
				i=i+1;
			}
			return index;
		}
		
	}
	public synchronized int getNrClientiServiti() {
		int i=-maxNoServers;
		for(Server s:servers)
			i=i+s.nrClienti;
		return i;
	}
	public synchronized int getClientiScheduler() {
		int sum=0;
		for(Server s:servers) {
			sum=sum+s.getSize();
			}
		return sum;
	}
	public Task[][] getAllTassks(){
		Task[][] t=new Task[maxNoServers][maxTasksPerServer];
		int i,j;
		for( i=0;i<maxNoServers;i++)
			for( j=0;j<maxTasksPerServer;j++)
				t[i][j]=new Task(0,0);
		 i=0;
		for(Server s:servers) {
			Task[] aux=s.getTasks();
			for(j=0;j<aux.length;j++)
				t[i][j]=aux[j];
			i=i+1;
		}
		return t;
	}
}