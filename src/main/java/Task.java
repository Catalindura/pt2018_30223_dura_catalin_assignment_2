
public class Task {
	private int arrivalTime;
	private int processingTime;
	Task(int a,int p){
		arrivalTime=a;
		processingTime=p;
	}
	public void setArrival(int x) {
		arrivalTime=x;
	}
	public void setProcessing(int x) {
		processingTime=x;
	}
	public int getArrivalTime() {
		return arrivalTime;
}
	public int getProcessingTime() {
		return processingTime;
	}
	public String toString() {
		return arrivalTime+" "+processingTime+"";
	}
}
