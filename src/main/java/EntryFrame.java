import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class EntryFrame {
	
	public static void main(String[] argv) {
		String s[]= {"Timp","Lungime Cozi"};
		final JComboBox box=new JComboBox (s);
		JFrame f=new JFrame();
		f.setSize(300,300);
		JPanel p=new JPanel();
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JPanel p4=new JPanel();
		JPanel p5=new JPanel();
		JPanel panel=new JPanel();
		panel. setLayout (new BoxLayout (panel, BoxLayout . Y_AXIS ));
		JLabel l1=new JLabel("Introduceti numarul de cozi:");
		JLabel l2=new JLabel("Introduceti numarul de clienti");
		JLabel l3=new JLabel("Introduceti Timpul limita");
		JLabel l4=new JLabel("Introduceti timpul minim de procesare:");
		JLabel l5=new JLabel("Introduceti timpul maxim de procesare:");
	    final JTextField t1=new JTextField(3);
	    final JTextField t2=new JTextField(3);
	    final JTextField t3=new JTextField(3);
	    final JTextField t4=new JTextField(3);
	    final JTextField t5=new JTextField(3);
		JButton b=new JButton("Start Market");
		p.add(l1);
		p.add(t1);
		p1.add(l2);
		p1.add(t2);
		p2.add(l3);
		p2.add(t3);
		p3.add(l4);
		p3.add(t4);
		p4.add(l5);
		p4.add(t5);
		p5.add(box);
		p5.add(b);
		panel.add(p);
		panel.add(p1);
		panel.add(p2);
		panel.add(p3);
		panel.add(p4);
		panel.add(p5);
		b.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int i=box.getSelectedIndex();  
				SimulationManager m=new SimulationManager(Integer.parseInt(t1.getText()),Integer.parseInt(t2.getText()),Integer.parseInt(t3.getText()),Integer.parseInt(t4.getText()),Integer.parseInt(t5.getText()),i);

				Thread t=new Thread(m);
				t.start();
				
			}
			});
		f.add(panel);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		
	}
}